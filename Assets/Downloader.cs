﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class Downloader : MonoBehaviour
{

    public ImageTargetController DogTarget;
    public ImageTargetController ChickenTarget;
    public ImageTargetController PenguinTarget;
    public Button DogButton;
    public Button ChickenButton;
    public Button PenguinButton;


    void Start() 
    {
      //UpdateInfo();

    }



    public void getDog() 
    {
        StartCoroutine(LoadAsset("Dog",DogTarget));
    }
    
    public void getChicken()
    {
        StartCoroutine(LoadAsset("Chicken",ChickenTarget));
    }
    
    public void getPenguin()
    {
        StartCoroutine(LoadAsset("Penguin",PenguinTarget));
    }


    //public void UpdateInfo(){
    //    StartCoroutine(sizeAsync("Chicken",ChickenButton));
    //    StartCoroutine(sizeAsync("Dog",DogButton));
    //    StartCoroutine(sizeAsync("Penguin",PenguinButton));
    //}

    //public IEnumerator sizeAsync(string name, Button button)
    //{
    //    AsyncOperationHandle<long> handle = Addressables.GetDownloadSizeAsync(name);
    //    yield return handle;
    //    if (handle.Status == AsyncOperationStatus.Succeeded)
    //    {
    //        long size = handle.Result;
    //        if(size==0){
    //        button.GetComponentInChildren<Text>().text+="(loaded)";
    //        }
    //        Addressables.Release(handle);
    //    }
    //}

    IEnumerator LoadAsset(string name, ImageTargetController controller)
    {

        var asset = Addressables.LoadAssetAsync<GameObject>(name);
        yield return asset;

        if (asset.Result == null)
        {
            Debug.Log("Failed to load Asset!");
            yield break;
        }
        
     
        
        Transform pref = Instantiate(asset.Result, new Vector3(0, 0, 0), Quaternion.identity).transform;

        pref.SetParent(controller.transform);
        pref.rotation = Quaternion.Euler(-90, 0, 0);
        pref.localPosition = Vector3.zero;
        //UpdateInfo();

    }
}
